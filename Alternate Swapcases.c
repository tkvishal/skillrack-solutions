#include<stdio.h>
#include <stdlib.h>

int main() {
    while(1) {
        char s[50];
        scanf("%s",s);
        for(int i = 0; s[i] != '\0'; i++) {
            if((i+1)%2) {
                printf("%c",toupper(s[i]));
            }
            else {
                printf("%c",tolower(s[i]));
            }
        }
        if(getchar() == '\n') {break;} else {printf(" ");}
    }
}