#include<stdio.h>
#include <stdlib.h>

int main() {
    char s1[101],s2[101]; scanf("%s",s1); scanf("%s",s2);
    int cnt = 0;
    int c1[256] = {0},c2[256] = {0};
    for(int i = 0; s1[i] != '\0'; i++) {
        c1[s1[i]]++;
    }
    for(int i = 0; s2[i] != '\0'; i++) {
        c2[s2[i]]++;
    }
    for(int i = 0; i < 256; i++) {
        cnt += c1[i] > c2[i] ? c2[i]:c1[i];
    }
    printf("%d",cnt);
}