#include<stdio.h>
#include <stdlib.h>

void swap(int *x,int *y) {
    int t = *x;
    *x = *y;
    *y = t;
}

void sort(int arr[], int s) {
    int i,j;
    for(i = 0; i < s-1; i++) {
        for(j = 0; j < s-i-1; j++) {
            if(arr[j] > arr[j+1]) {
                swap(&arr[j],&arr[j+1]);
            }
        }
    }
}

int dups(int a[],int n) {
    int j = 0;
    for(int i = 0; i < n-1; i++) {
        if(a[i] != a[i+1]) {
            a[j++] = a[i];
        }
    }
    a[j++] = a[n-1];
    return j;
}

int main() {
    int n,m; scanf("%d %d",&n,&m);
    int a[n],b[m],c = 0;
    for(int i = 0; i < n; i++) {
        scanf("%d",&a[i]);
    }
    for(int i = 0; i < m; i++) {
        scanf("%d",&b[i]);
    }
    sort(a,n); sort(b,m);
    n = dups(a,n); m = dups(b,m);
    int i = 0, k = 0, j = 0;
    while(i < n && j < m) {
        if(a[i] < b[j]) {
            //printf("%d ",a[i]);
            i++; k++; c++;
        } else if(a[i] > b[j]) {
            //printf("%d ",b[j]);
            c++;
            k++; j++;
        } else {
            i++; j++;
        }
    }
    while(i < n) {
        //printf("%d ",a[i]);
        c++; i++; k++;
    }
    while(j < m) {
        //printf("%d ",b[j]);
        c++; j++; k++;
    }
    printf("\n%d",c);
}