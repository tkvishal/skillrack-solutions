//setup with ece vars
document.addEventListener('DOMContentLoaded', function() {
  changeece()
}, false);

//get the dropdown boxes to work
$(".dropdown-menu li a").click(function() {
  $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
  $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
});

//change dept trigger
document.getElementById("ECE").onclick = changeece;
document.getElementById("CSE").onclick = changecse;
gradepts = []
subjects = []

//change to cse
function changecse() {
  subjects = ["Technical English", "Engineering Mathematics - II", "Physics for Information Science", "Basic Electrical,Electronics and Measurement Engineering", "Environmental Science and Engineering", "Programming in C", "Engineering Practices Laboratory", "C Programming Laboratory"]
  gradepts = [4, 4, 3, 3, 3, 3, 2, 2]
  for (i = 0; i < 8; i++) {
    document.getElementsByClassName("s")[i].innerText = subjects[i]
  }
}

//change to ece
function changeece() {
  subjects = ["Technical English", "Engineering Mathematics - II", "Physics for Electronics Engineering", "Basic Electrical and Instrumentation Engineering", "Circuit Analysis", "Electronic devices", "Circuits and Devices Lab", "Engineering Practices Lab"]
  gradepts = [4, 4, 3, 3, 4, 3, 2, 2]
  for (i = 0; i < 8; i++) {
    document.getElementsByClassName("s")[i].innerText = subjects[i]
  }
}

//debug
/*document.addEventListener("click",function p() {
  console.log(gradepts)
  console.log(subjects)
})
*/

//parse grade to grade points(common)
var parser = {
  "O": 10,
  "A+": 9,
  "A": 8,
  "B+": 7,
  "B": 6
}

//gather info and validate
function gatherinfo() {
  var grades = []
  for (i = 1; i <= 8; i++) {
    grades.push(document.getElementsByClassName("sub" + String(i))[0].innerText.trim())
  }
  if (validate(grades)) {
    gradeparsed = parse(grades)
    var GPA = calculate(gradeparsed)
    document.getElementById("GPA").innerText = "Your GPA is " + String(GPA)
  } else {
    window.alert("Please fill in all the details")
  }
}

//activate on click submit btn
document.getElementById("calc").onclick = gatherinfo;

//validate the grades submitted
function validate(grd) {
  for (i = 0; i < 8; i++) {
    if (grd[i] == "Select Grade") {
      return false
    }
  }
  return true
}

//parse grades to grade points
function parse(grd) {
  grades = []
  for (i = 0; i < 8; i++) {
    grades.push(parser[grd[i]])
  }
  return grades
}

//calculate GPA
function calculate(grd) {
  var num = 0,
    den = 0
  for (i = 0; i < 8; i++) {
    num += gradepts[i] * grd[i];
    den += gradepts[i];
  }
  return num / den
}
