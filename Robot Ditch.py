"""A robot is programmed to move forward F meters and backwards again, say B meters, in a straight line. The Robot covers 1 meter in T units of time. On Robot's path there is a ditch at a distance FD from initial position in forward direction as well as a ditch at a distance BD from initial position in backward direction. This forward and backward movement is performed repeatedly by the Robot.
Your task is to calculate amount of time taken, before the Robot falls in either ditch, if at all it falls in a ditch.
Input Format:
First line contains total number of test cases, denoted by N
Next N lines, contain a tuple containing 5 values delimited by space
F B T FD BD, where
F denotes forward displacement in meters
B denotes backward displacement in meters
T denotes time taken to cover 1 meter
FD denotes distance from Robot's starting position and the ditch in forward direction
BD denotes distance from Robot's starting position and the ditch in backward direction
Output Format:
For each test case print time taken by the Robot to fall in the ditch and also state which ditch he falls into. Print F for forward and B for backward. Both the outputs must be delimited by whitespace
OR
Print No Ditch if the Robot does not fall in either ditch
Constraints:
First move will always be in forward direction
1 <= N <= 100
All input values must be positive integers only
Sample Input and Output
SNo.	Input	Output
1	
3
9 4 3 13 10
9 7 1 11 13
4 4 3 8 12

63 F
25 F
No Ditch
2	
5
8 4 7 11 22
4 5 4 25 6
4 9 3 6 29
7 10 6 24 12
10 10 1 9 7

133 F
216 B
231 B
408 B
9 F"""
#code starts...
n=int(input());ls=[]
for i in range(n):
    ls.append(list(map(int,input().split())))
for i in range(len(ls)):
    f=ls[i][0];b=ls[i][1];t=ls[i][2];fd=ls[i][3];bd=ls[i][4];dp=0;st=0;flag=0;
    while(1):
        if(f==b and f<fd):
            print("NO DITCH");break;
        else:
            for i in range(f):
                dp+=1;st+=1
                if(dp==fd and f>=b):
                    print((st)*t,"F");
                    flag=1;break;
            if(flag==1):    break;
            for k in range(b):
                dp-=1;st+=1
                if(abs(dp)==bd and b>=f):
                    print((st*t),"B")
                    flag=1;break;