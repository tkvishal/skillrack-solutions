from flask_wtf import Form
from wtforms import StringField, PasswordField, TextAreaField
from wtforms.validators import (DataRequired, Regexp, ValidationError, Email, Length, EqualTo)
from models import User
def user_registered(form, field):
    if User.select().where(User.username == field.data).exists():
        raise ValidationError('User-ID already Registered! Please Try something Unique')
def email_registered(form, field):
    if User.select().where(User.email == field.data).exists():
        raise ValidationError('e-mail ID already Registered!')
class RegisterForm(Form):
    username = StringField('Username',validators=[DataRequired(), Regexp(r'^[a-zA-Z0-9_]+$', 
                message=("Username should be one word, it can have letters,numbers,underscores but not special characters")),user_registered])
    email = StringField('Email', validators=[DataRequired(),Email(),email_registered])
    password = PasswordField('Password', validators=[DataRequired(),Length(min=2),
            EqualTo('password2', message='Passwords must match')])
    password2 = PasswordField('Confirm Password',validators=[DataRequired()])
class LoginForm(Form):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
class PostForm(Form):
    content = TextAreaField("Write a Story", validators=[DataRequired()])