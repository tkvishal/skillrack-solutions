import os
from flask import Flask, render_template, request, redirect, session
from flask_sqlalchemy import SQLAlchemy 
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/HD EDIT/Downloads/flask login/flask app/login.db'
app.config['SECRET_KEY'] = 'thisissecretcodedonotrevealthistohackersplease'

db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
login_manager.login_message = 'login to continue'

#the user data base manager
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), unique=True)

#to get the user id
@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

'''@app.route('/')
def index():
	user = User.query.filter_by(username = 'vishal').first()
	login_user(user)
	return 'Now you have logged in'
'''

#login page
@app.route('/login')
def login():
	session['next'] = request.args.get('next')
	return render_template('login.html')

#to validate login credentials
@app.route('/logmein', methods = ['POST'])
def logmein():
	uname = request.form['username']
	user = User.query.filter_by(username = uname).first()

	if not user:
		return 'you thief, your name is not in the database!!'

	login_user(user, remember = True)
	return redirect('home')

#to logout
@app.route('/logout')
@login_required
def logout():
	logout_user()
	return 'Now you have logged out'

#home
@app.route('/home')
@login_required
def home():
	return 'Welcome  ' + current_user.username

app.run(debug=True)