from flask import (Flask, g, render_template, flash, redirect, url_for, abort)
from flask.ext.bcrypt import check_password_hash
from flask.ext.login import (LoginManager, login_user, logout_user,login_required, current_user)
import register
app = Flask(__name__)
@app.route('/register', methods=('GET', 'POST'))
def register():
    form = register.RegisterForm()
    if form.validate_on_submit():
        flash("Thank-You for Registering!", "Success! write your 1st story")
        models.User.create_user(username=form.username.data,email=form.email.data,password=form.password.data)
        return redirect(url_for('index'))
    return render_template('register.html', form=form)