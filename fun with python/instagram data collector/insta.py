#to parse the HTML
from bs4 import BeautifulSoup
#to make a headless chrome web browser
import selenium.webdriver as wb

from selenium.webdriver.chrome.options import Options
import sys
import time
#emoji is not supported in CLI so, convert it into boxes
translate_emoji = dict.fromkeys(range(0x10000, sys.maxunicode + 1), "\ufffd")

#init browser
options = Options()
options.add_argument('--headless')
options.add_argument('--disable-gpu')

#init loop
ctrl = 0
while(1):
    s = input("Enter instagram userID: ")

    url = "https://www.instagram.com/"+s+"/"

    #https://chromedriver.storage.googleapis.com/2.40/chromedriver_win32.zip
    #download this file and link the path of .exe in driver variable
    driver = wb.Chrome(r"C:\Users\HD EDIT\Downloads\chromedriver.exe", chrome_options=options)
    driver.get(url)

    #soup out all the respective contents..
    soup = BeautifulSoup(driver.page_source,"html5lib")
    info = soup.find_all("span",{"class" : "g47SY"})
    name = soup.find("h1",{"class" : "rhpdm"})
    desc = soup.find("div",{"class" : "-vDIg"}).find("span")

    #loading animation
    '''print("~loading~\n")
    frames = u"\u2610"
    for i in range(50):
        time.sleep(1)
        sys.stdout.write(frames)
        sys.stdout.flush()
        print("\ndone\n")'''
    
    #print 'em    
    print("Name: "+name.text.translate(translate_emoji))
    print("Number of posts: " + info[0].text)
    print("Number of followers: " + info[1].text)
    print("Number of following: " + info[2].text)
    if(desc == None):
        print("{} does not have any description".format(s))
    else:
        print("Description: \n" + desc.text.translate(translate_emoji))
    print("\n\n~Enter 1 to continue 0 to exit~")

    #ask for continuation
    ctrl = int(input())
    if(ctrl == 0):
        driver.quit()
        break
    else:
        driver.quit()
