var config = {
	    apiKey: "AIzaSyBXTgH5hTkEPmG8WfuQnzD5Ky2qAXpEFf4",
	    authDomain: "sample-2be05.firebaseapp.com",
	    databaseURL: "https://sample-2be05.firebaseio.com",
	    projectId: "sample-2be05",
	    storageBucket: "sample-2be05.appspot.com",
	    messagingSenderId: "125473245146"
}

firebase.initializeApp(config)
var database = firebase.database()
var ref = firebase.database().ref("messages/");

function clear_and_write(ref, data) {
	//pass as json dict {key:'value'}
	ref.set(data)
}

function clear_data(ref) {
	ref.set({placeholder:'nothing'})
}

function push_data(ref,data) {
	for(i = 0; i < data.length; i++) {
		ref.push(data[i])
	}
}

function update_data(ref,data) {
	ref.update(data)
}

function get_structure(ref) {
	ref.on("value", function(snapshot) {
	   return snapshot.val()
	}, function (error) {
	   return error.code
	});
}

data = [{name:'santhosh'},{name:'vishal'}]
//get data by 'name' attribute
push_data(ref,data)
ref.orderByChild('name').on("child_added", function(data) {
	console.log(data.val().name);
});